<?php
class Controllers_Users extends RestController {
	private $_con;
	private function DBconnect(){
		//$this->_con=mysqli_connect("localhost","ronih_test1_a","test1_a","ronih_test1");
		$this->_con=mysqli_connect("localhost","morta","56925","morta_REST_API");
	}
	public function get() {
		$this->DBconnect();
		//var_dump($this->request['params']['id']);
		//die();
		if (isset($this->request['params']['id'])){
			if(!empty($this->request['params']['id'])) {
				$sql="SELECT * FROM users WHERE id=?";				
				$stmt = $this->_con->prepare($sql);
				$stmt->bind_param("s",$this->request['params']['id']);
				$stmt->execute();
				$stmt->store_result();				
				$stmt->bind_result($id, $name,$email);
				$stmt->fetch();
				$result = ['id'=>$id,'name'=>$name,'email'=>$email];
				$this->response = array('result' =>$result );
				$this->responseStatus = 200;
			} else {
				/*$this->response = array('result' =>'Getting multiple users not yet implemented');*/
				$sql="SELECT * FROM users";
				$usersSqlResult = mysqli_query($this->_con, $sql);
				$users = [];
					While ($user = mysqli_fetch_array($usersSqlResult)){
						$users[] = ['name' => $user['name'], 'email' => $user['email']];
					}
					$this->response = array ('result' =>$users);
					$this->responseStatus = 200; //sucsses status 
			}	
		}else{
			$this->response = array('result' =>'Wrong parameters for users' );
			$this->responseStatus = 200;			
		}
	}
	public function post() {
	$this->DBconnect();
		if(!empty($this->request['params']['payload'])) {
				$user []= json_decode($this->request['params']['payload'],true);
				$name=$user[0]['name'];
				$email=$user[0]['email'];
				$sql=mysqli_query($this->_con,"INSERT INTO `users` (`name`, `email`) VALUES ('$name','$email')");
				$sql2="SELECT * FROM `users` ORDER BY `id` DESC LIMIT 1";
				$result = mysqli_query($this->_con, $sql2);
				if (mysqli_num_rows($result) > 0) {
					while($row = mysqli_fetch_array($result)) {
						$this->response=array("User ID is: ".$row['id']);
					}
				}
			}
	else{
		$this->response = array('result' => 'no post implemented for users');
		$this->responseStatus = 201;
		}
	}
	public function put() {
		$this->response = array('result' => 'no put implemented for users');
		$this->responseStatus = 200;
	}
	public function delete() {
		$this->response = array('result' => 'no delete implemented for users');
		$this->responseStatus = 200;
	}
}
